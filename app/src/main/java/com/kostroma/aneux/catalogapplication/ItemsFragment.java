package com.kostroma.aneux.catalogapplication;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ItemsFragment extends Fragment {

    SwipeRefreshLayout swipeRefreshLayout;
    ListView lvItems;

    ArrayList<Map<String, String>> itemsData;
    ItemsAdapter adapter;

    FragmentActivity context;
    String categoryid, json;

    ParsingTask parsingTask;
    RefreshTask refreshTask;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context.setTitle(getArguments().getString("category"));
        categoryid = getArguments().getString("categoryid");
        json = getArguments().getString("json");
        View view = inflater.inflate(R.layout.fragment_items, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeToRefresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                refreshTask = new RefreshTask();
                refreshTask.execute();
            }
        });
        lvItems = (ListView) view.findViewById(R.id.lvItems);
        itemsData = new ArrayList<>();
        adapter = new ItemsAdapter(context, itemsData);
        lvItems.setAdapter(adapter);

        parsingTask = new ParsingTask();
        parsingTask.execute(json);
        return view;
    }

    class ParsingTask extends AsyncTask<String, Void, ArrayList<Map<String, String>>> {

        @Override
        protected ArrayList<Map<String, String>> doInBackground(String... params) {
            ArrayList<Map<String, String>> data = null;
            Map<String, String> m;
            try {
                JSONObject jsonObject = new JSONObject(params[0]);
                JSONArray items = jsonObject.getJSONArray("items");
                data = new ArrayList<>();
                for (int i = 0; i < items.length(); i++) {
                    JSONObject item = items.getJSONObject(i);
                    if (categoryid.equals(item.getString("categoryid"))) {
                        m = new HashMap<>();
                        m.put("name", item.getString("name"));
                        m.put("weight", item.getString("weight"));
                        m.put("metal", item.getString("metal"));
                        m.put("proba", item.getString("proba"));
                        JSONArray image = item.getJSONArray("images");
                        m.put("image", image.getString(0));
                        data.add(m);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<Map<String, String>> result) {
            super.onPostExecute(result);
            itemsData.clear();
            itemsData.addAll(result);
            adapter.notifyDataSetChanged();
            if (swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);
        }

    }

    class RefreshTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!Utils.isNetworkAvailable(context)) {
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(context, getResources().getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = "";
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("http://pars-gold.ru/Exch").openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.connect();
                InputStream inputStream = httpURLConnection.getInputStream();
                StringBuilder stringBuilder = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = bufferedReader.readLine()) != null)
                    stringBuilder.append(line);
                inputStream.close();
                result = stringBuilder.toString();
                Utils.saveJSONToFile(context, result);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            parsingTask = new ParsingTask();
            parsingTask.execute(result);
        }

    }

}
