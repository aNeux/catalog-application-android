package com.kostroma.aneux.catalogapplication;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CategoriesFragment extends Fragment {

    SwipeRefreshLayout swipeRefreshLayout;
    ListView lvCategories;
    LinearLayout llLoading, llError;
    Button btnRetry;

    String json = "";
    ArrayList<Map<String, String>> categoriesData;
    SimpleAdapter adapter;

    ParsingTask parsingTask;
    RefreshTask refreshTask;
    FragmentActivity context;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categories, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeToRefresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                refreshTask = new RefreshTask();
                refreshTask.execute();
            }
        });
        lvCategories = (ListView) view.findViewById(R.id.lvCategories);
        lvCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((ContainerActivity) context).showItemsFragment(categoriesData.get(position).get("category"), categoriesData.get(position).get("categoryid"), json);
            }
        });
        if (categoriesData == null) categoriesData = new ArrayList<>();
        String[] from = { "category" };
        int[] to = { android.R.id.text1 };
        adapter = new SimpleAdapter(context, categoriesData, android.R.layout.simple_list_item_1, from, to);
        lvCategories.setAdapter(adapter);

        llLoading = (LinearLayout) view.findViewById(R.id.llLoading);
        llError = (LinearLayout) view.findViewById(R.id.llError);
        btnRetry = (Button) view.findViewById(R.id.btnRetry);
        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llLoading.setVisibility(View.VISIBLE);
                llError.setVisibility(View.GONE);
                refreshTask = new RefreshTask();
                refreshTask.execute();
            }
        });
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (categoriesData.size() == 0) {
            if (Utils.isCacheExists(context)) {
                json = Utils.readJSONFromFile(context);
                parsingTask = new ParsingTask();
                parsingTask.execute(json);
                context.setTitle(context.getResources().getString(R.string.app_name) + ": Категории");
            } else {
                llLoading.setVisibility(View.VISIBLE);
                swipeRefreshLayout.setVisibility(View.GONE);
                refreshTask = new RefreshTask();
                refreshTask.execute();
            }
        } else {
            ((ContainerActivity) context).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            context.setTitle(context.getResources().getString(R.string.app_name) + ": Категории");
        }
    }

    class ParsingTask extends AsyncTask<String, Void, ArrayList<Map<String, String>>> {

        @Override
        protected ArrayList<Map<String, String>> doInBackground(String... params) {
            ArrayList<Map<String, String>> data = null;
            Map<String, String> m;
            try {
                JSONObject jsonObject = new JSONObject(params[0]);
                JSONArray categories = jsonObject.getJSONArray("categories");
                data = new ArrayList<>(categories.length());
                for (int i = 0; i < categories.length(); i++) {
                    JSONObject category = categories.getJSONObject(i);
                    m = new HashMap<>();
                    m.put("categoryid", category.getString("categoryid"));
                    m.put("category", category.getString("category"));
                    data.add(m);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<Map<String, String>> result) {
            super.onPostExecute(result);
            categoriesData.clear();
            categoriesData.addAll(result);
            adapter.notifyDataSetChanged();
            if (llLoading.getVisibility() == View.VISIBLE)
            {
                context.setTitle(context.getResources().getString(R.string.app_name) + ": Категории");
                swipeRefreshLayout.setVisibility(View.VISIBLE);
                llLoading.setVisibility(View.GONE);
            }
            else if (swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);
        }

    }

    class RefreshTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!Utils.isNetworkAvailable(context)) {
                if (llLoading.getVisibility() == View.VISIBLE) {
                    llError.setVisibility(View.VISIBLE);
                    llLoading.setVisibility(View.GONE);
                }
                else {
                    swipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(context, getResources().getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                }
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = "";
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("http://pars-gold.ru/Exch").openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.connect();
                InputStream inputStream = httpURLConnection.getInputStream();
                StringBuilder stringBuilder = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = bufferedReader.readLine()) != null)
                    stringBuilder.append(line);
                inputStream.close();
                result = stringBuilder.toString();
                json = result;
                Utils.saveJSONToFile(context, json);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            parsingTask = new ParsingTask();
            parsingTask.execute(result);
        }

    }

}
