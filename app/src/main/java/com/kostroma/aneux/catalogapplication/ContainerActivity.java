package com.kostroma.aneux.catalogapplication;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ContainerActivity extends AppCompatActivity {

    CategoriesFragment categoriesFragment;
    ItemsFragment itemsFragment;
    FragmentManager fManager;
    FragmentTransaction fTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        categoriesFragment = new CategoriesFragment();
        fManager = getSupportFragmentManager();
        fTransaction = fManager.beginTransaction();
        fTransaction.add(android.R.id.content, categoriesFragment);
        fTransaction.commit();
    }

    public void showItemsFragment(String category, String categoryid, String json) {
        Bundle bundle = new Bundle();
        bundle.putString("category", category);
        bundle.putString("categoryid", categoryid);
        bundle.putString("json", json);
        itemsFragment = new ItemsFragment();
        itemsFragment.setArguments(bundle);
        fTransaction = fManager.beginTransaction();
        fTransaction.replace(android.R.id.content, itemsFragment);
        fTransaction.addToBackStack(null);
        fTransaction.commit();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        getSupportFragmentManager().popBackStack();
        return true;
    }

}
