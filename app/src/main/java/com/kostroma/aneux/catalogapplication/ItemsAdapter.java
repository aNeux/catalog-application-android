package com.kostroma.aneux.catalogapplication;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Map;

public class ItemsAdapter extends BaseAdapter {

    Context context;
    Resources resources;
    LayoutInflater lInflater;
    ArrayList<Map<String, String>> objects;

    public ItemsAdapter(Context context, ArrayList<Map<String, String>> items) {
        this.context = context;
        objects = items;
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        resources = context.getResources();
    }

    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) view = lInflater.inflate(R.layout.listview_item, parent, false);

        Map<String, String> m = objects.get(position);
        ((TextView) view.findViewById(R.id.tvName)).setText(m.get("name"));
        ((TextView) view.findViewById(R.id.tvMetal)).setText(resources.getString(R.string.listview_item_metal) + ": " + m.get("metal"));
        ((TextView) view.findViewById(R.id.tvWeight)).setText(resources.getString(R.string.listview_item_weight) + ": " + m.get("weight") + " гр");
        ((TextView) view.findViewById(R.id.tvProba)).setText(resources.getString(R.string.listview_item_proba) + ": " + m.get("proba"));
        Picasso.with(context).load(m.get("image")).placeholder(R.drawable.placeholder).into((ImageView) view.findViewById(R.id.ivImage));
        return view;
    }

}
